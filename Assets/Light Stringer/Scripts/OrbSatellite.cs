﻿using UnityEngine;
using System.Collections;

public class OrbSatellite : MonoBehaviour {
	public int orbitSpeed = 40;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.RotateAround(gameObject.transform.parent.transform.position, gameObject.transform.parent.transform.up, orbitSpeed * Time.deltaTime);
	}
}
