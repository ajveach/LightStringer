﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : Singleton<GameController> {
	public GameController(){}

	private bool gameIsOver = false;

	public string gameMode = "Time Challenge";
	public TimeChallenge timeChallengeHandler;

	public ObjectPool spherePool = new ObjectPool();

	public int totalPiecesRemoved = 0;
	public int longestChain = 0;
	public int score = 0;
	public float timer = 0.0f;

	public int gridWidth = 6;
	public int gridHeight = 6;
	public float sphereDiameter = 1;
	public float sphereSpacing = 1;

	// Grid values
	private Vector2 gridStart;

	public float settleSpeed = 5.0f;
	private List<gamePiece> settlingPieces = new List<gamePiece> ();
	public Color[] pieceColors;
	public GameObject[] piecePrefabs;

	private GameObject[,] grid;

	// Use this for initialization
	void Start () {

	}

	public void setGameMode(string mode){
		this.gameMode = mode;
		switch (gameMode) {
			case "Time Challenge":
				this.timeChallengeHandler = new TimeChallenge();
				break;
		}
	}

	public void startGame(){
		//Camera.main.orthographic = true;
		this.setGameMode ("Time Challenge");
		
		grid = new GameObject[gridWidth,gridHeight];
		//pieceColors = new Color[4];
		//this.generateColors ();
		this.buildInitialGrid (false);

		this.resetScore();
		this.gameIsOver = false;
		GUIController.Instance.disabled = false;
		TimeController.Instance.setTimer();
		TimeController.Instance.unpause();
	}

	public void resetScore(){
		totalPiecesRemoved = 0;
		longestChain = 0;
		score = 0;
		timer = 0;
	}

	public int generateScore(int score, int count){
		int increment = 0;
		switch (this.gameMode) {
			case "Time Challenge":
				increment = this.timeChallengeHandler.generateScore(score,count);
				break;
		}
		return increment;
	}
	
	// Update is called once per frame
	void Update () {
		// If dev build and user presses exit key close app
		if (Debug.isDebugBuild && Input.GetButtonUp ("Exit")) {
			Application.Quit();
		}

		// Settle pieces in list
		if (settlingPieces.Count > 0) {
			int gridDistance = 0;
			Vector3 modifiedPosition;
			List<gamePiece> piecesStopping = new List<gamePiece>();
			foreach(gamePiece piece in settlingPieces){
				gridDistance = 0;

				// If destination not set for piece, determine where it will end
				if(piece.settleDestination == -1.0f){
					// Determine how many empty spaces are below piece
					for(int col = piece.Y; col < GameController.Instance.gridHeight; col++){
						if(grid[piece.X,col] == null || !grid[piece.X,col].activeSelf){
							gridDistance++;
						}
					}

					if(gridDistance > 0){
						// Remove piece from previous location in grid
						grid[piece.X,piece.Y] = null;

						// Piece has room to fall. Determine grid to world distance translation
						piece.Y += gridDistance;
						piece.settleDestination = gridToWorldPosition(new Vector2(piece.X,piece.Y)).y;

						// Place piece in correct location in grid
						grid[piece.X,piece.Y] = piece.gameObject;
					}
				}

				// Move piece towards destination
				modifiedPosition = piece.transform.position;
				modifiedPosition.y -= settleSpeed * Time.deltaTime;
				if(modifiedPosition.y < piece.settleDestination){
					modifiedPosition.y = piece.settleDestination;
				}
				piece.transform.position = modifiedPosition;

				// If piece at destination, set settleDestination = -1 and remove it from list
				if(modifiedPosition.y == piece.settleDestination){
					piece.settleDestination = -1.0f;
					piecesStopping.Add (piece);
				}
			}

			// Remove pieces that are ready to stop
			if(piecesStopping.Count > 0){
				foreach(gamePiece piece in piecesStopping){
					settlingPieces.Remove (piece);
				}
				piecesStopping.Clear();
			}

			// Check if all pieces are settled. If so, show new pieces
			if(settlingPieces.Count == 0){
				this.showNewPieces();
			}
		}

		if(!this.gameIsOver){
			timer += Time.deltaTime;
		}
	}

	public void showNewPieces(){
		GameObject newPiece;
		gamePiece newPieceComponent;
		// this is fired when all pieces have finished settling
		// Starting from last row of grid, move up row by row. 
		for (int row = gridHeight - 1; row >= 0; row--) {
			// Loop through columns
			for(int col = 0; col < gridWidth; col++){
				if(grid[col,row] == null){
					newPiece = spherePool.retrieve();
					newPieceComponent = newPiece.GetComponent<gamePiece>();
					newPieceComponent.X = col;
					newPieceComponent.Y = row;

					// set new piece color randomly
					newPiece.renderer.material.SetColor("_Color", pieceColors[Random.Range (0,4)]);

					// Place new piece in correct location
					newPiece.transform.position = gridToWorldPosition(new Vector2(col,row));
					grid[col,row] = newPiece;
				}
			}
		}

		// With new pieces in place, check to make sure at least 1 move is available
		if (!this.validMoveRemaining ()) {
			Debug.Log ("No moves available");
			resetBoard();
		}
	}

	private void generateColors(){
		pieceColors [0] = Color.red;
		pieceColors [1] = Color.green;
		pieceColors [2] = Color.blue;
		pieceColors [3] = Color.yellow;
	}

	private void resetBoard(){
		// Pause game
		TimeController.Instance.pause ();

		// Trigger GUI element && start restart countdown
		GUIController.Instance.toggleBoardResetDisplay ();

		// Add all pieces to pool
		gamePiece pieceComponent;
		GameObject piece;
		// Starting from last row of grid, move up row by row. 
		for (int row = gridHeight - 1; row >= 0; row--) {
			// Loop through columns
			for (int col = 0; col < gridWidth; col++) {
				piece = grid[col,row];
				// Add item to object pool and hide
				GameController.Instance.spherePool.add (piece);
				// Remove it from grid
				pieceComponent = piece.GetComponent<gamePiece>();
				pieceComponent.removeFromGrid();
				pieceComponent.selected = false;
			}
		}

		// rebuild grid
		this.buildInitialGrid (true);
	}

	public void endBoardReset(){
		// Unpause at end of countdown
		TimeController.Instance.unpause ();
	}

	private void buildInitialGrid(bool resetBoard){
		gridStart.x = -(gridWidth * (sphereDiameter + sphereSpacing)) / 2 + ((sphereSpacing + sphereDiameter) / 2);
		gridStart.y = (gridHeight * (sphereDiameter + sphereSpacing)) / 2 - ((sphereSpacing + sphereDiameter) / 2);
		Vector2 currentGridPosition = new Vector2(0,0);
		Vector3 currentWorldPosition;
		GameObject newPiece;
		int pieceKey;
		gamePiece currentPieceController;
		for(int i = 0; i < gridWidth * gridHeight; i++){
			// Determine key used for color & prefab
			pieceKey = Random.Range (0,pieceColors.Length);

			if(i % gridWidth == 0 && i > 0){
				currentGridPosition.x = 0;
				currentGridPosition.y++;
			}
			currentWorldPosition = gridToWorldPosition(currentGridPosition);
			if(resetBoard){
				newPiece = spherePool.retrieve();
				newPiece.transform.position = currentWorldPosition;
			}
			else{
				//newPiece = Instantiate(Resources.Load("Sphere", typeof(GameObject)), currentWorldPosition, Quaternion.identity) as GameObject;
				newPiece = Instantiate(piecePrefabs[pieceKey], currentWorldPosition, Quaternion.identity) as GameObject;
			}

			currentPieceController = newPiece.GetComponent<gamePiece>();
			currentPieceController.X = (int)currentGridPosition.x;
			currentPieceController.Y = (int)currentGridPosition.y;
			currentPieceController.selected = false;
			newPiece.renderer.material.SetColor("_Color", pieceColors[pieceKey]);

			grid[(int)currentGridPosition.x , (int)currentGridPosition.y] = newPiece;
			currentGridPosition.x++;
		}
	}

	public void toggleEndOfGame(){
		this.gameIsOver = true;
		InputController.Instance.endChain();
		TimeController.Instance.pause ();

		updateRecords();
	}

	private void updateRecords(){
		// Save high score
		int highScoreRecord = PlayerPrefs.GetInt ("highScore");
		int longestChainRecord = PlayerPrefs.GetInt ("longestChain");
		if (highScoreRecord < this.score) {
			PlayerPrefs.SetInt ("highScore", this.score);
			GUIController.Instance.newHighScore = true;
		} else {
			GUIController.Instance.newHighScore = false;
		}

		if (longestChainRecord < this.longestChain) {
			PlayerPrefs.SetInt ("longestChain", this.longestChain);
			GUIController.Instance.newLongestChain = true;
		} else {
			GUIController.Instance.newLongestChain = false;
		}
	}

	public GameObject fetchSphere(int x, int y){
		if (x < gridWidth && x >= 0 && y < gridHeight && y >= 0) {
			if (grid [x, y]) {
					return grid [x, y];
			}
		}
		return null;
	}

	public void removeFromGrid(int x, int y){
		grid [x, y] = null;
	}

	public void addToSettlingList(gamePiece piece){
		if(!settlingPieces.Contains(piece)){
			settlingPieces.Add (piece);
		}
	}

	public bool isInSettlingList(gamePiece piece){
		if (settlingPieces.Contains (piece)) {
			return true;
		}
		return false;
	}

	public int settlingPiecesCount(){
		return settlingPieces.Count;
	}

	public Vector3 gridToWorldPosition(Vector2 gridPos){
		Vector3 worldPos = new Vector3 (0, 0, 0);
		worldPos.x = gridStart.x + (gridPos.x * (sphereDiameter + sphereSpacing));
		worldPos.y = gridStart.y - (gridPos.y * (sphereDiameter + sphereSpacing));

		return worldPos;
	}

	public bool isOver(){
		return this.gameIsOver;
	}

	public bool validMoveRemaining(){
		GameObject thisPiece;
		Color thisColor;
		// Starting from last row of grid, move up row by row. 
		for (int row = gridHeight - 1; row >= 0; row--) {
			// Loop through columns
			for (int col = 0; col < gridWidth; col++) {
				// Loop through all surrounding pieces... if any have matching color return true
				thisPiece = grid[col,row];
				gamePiece thisPC = thisPiece.GetComponent<gamePiece>();
				int pieceX = thisPC.X;
				int pieceY = thisPC.Y;
				thisColor = thisPiece.renderer.material.GetColor ("_Color");
				
				// Above
				if (pieceY > 0) {
					if(grid[pieceX,pieceY - 1].renderer.material.GetColor("_Color") == thisColor){
						return true;
					}
				}
				
				// Right
				if (pieceX < gridWidth - 1) {
					if(grid[pieceX + 1,pieceY].renderer.material.GetColor("_Color") == thisColor){
						return true;
					}
				}
				
				// Bottom
				if (pieceY < gridHeight - 1) {
					if(grid[pieceX, pieceY + 1].renderer.material.GetColor("_Color") == thisColor){
						return true;
					}
				}
				
				// Left
				if (pieceX > 0) {
					if(grid[pieceX - 1,pieceY].renderer.material.GetColor("_Color") == thisColor){
						return true;
					}
				}
			}
		}
		return false;
	}
}
