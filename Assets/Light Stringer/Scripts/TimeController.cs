﻿using UnityEngine;
using System.Collections;

public class TimeController : Singleton<TimeController> {
	public TimeController(){}

	public float timeChallengeStartTime = 30.0f;

	private bool paused = true;
	private float currentTime = 0.0f;

	// Use this for initialization
	void Start () {}

	public void setTimer(){
		// Setup timer based on game mode
		switch(GameController.Instance.gameMode){
			case "Time Challenge":
				currentTime = timeChallengeStartTime;
				break;
			case "Player Challenge":
				
				break;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(!paused){
			currentTime -= Time.deltaTime;
		}

		if(currentTime <= 0){
			if(!GameController.Instance.isOver()){
				GameController.Instance.toggleEndOfGame();
			}
		}
	}

	public void addToTime(float increment){
		this.currentTime += increment;
	}

	public int current(){
		return Mathf.CeilToInt(currentTime);
	}

	public void pause(){
		paused = true;
	}

	public void unpause(){
		paused = false;
	}

	public bool isPaused(){
		return paused;
	}
}
