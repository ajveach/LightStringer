﻿using UnityEngine;
using System.Collections;

public class gamePiece : MonoBehaviour {
	public bool selected = false;
	public int X;
	public int Y;
	public int distanceToSettle = 0;
	public float settleDestination = -1.0f;

	private GameObject GC;
	private InputController inputController;

	void Start(){
		GC = GameObject.FindGameObjectWithTag ("GameController");
		inputController = GC.GetComponent<InputController> ();
	}

	void OnMouseDown(){
		if(!TimeController.Instance.isPaused()){
			inputController.startChain(gameObject);
		}
	}

	void OnMouseEnter(){
		if (inputController.creatingChain) {
			inputController.addSphereToChain (gameObject);
		}
	}

	void OnMouseUp(){
		inputController.endChain ();
	}

	public void removeFromGrid(){
		GameController.Instance.removeFromGrid (this.X, this.Y);
	}
}
