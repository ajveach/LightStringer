﻿using UnityEngine;
using System.Collections;

public class LineDraw : MonoBehaviour {
	private InputController CinputController;

	public Shader lineMaterial;

	public Color color = Color.white;
	public float width = 0.2f;
	public int numberOfPoints = 20;

	void Start () {
		CinputController = GetComponent<InputController> ();
	}

	public void draw () {
		// If chain has 2 pieces draw a basic straight line
		if (CinputController.currentChain.Count == 2) {
			drawSingleSegment(CinputController.currentChain[0],CinputController.currentChain[1]);
		}
		// If chain has 3+ pieces draw a bezier curve
		else if (CinputController.currentChain.Count > 2) {
			GameObject startSphere;
			GameObject middleSphere;
			GameObject endSphere;
			Vector3 p0;
			Vector3 p1;
			Vector3 p2;
			float t;
			Vector3 position;
			for(int k = 0; k < CinputController.currentChain.Count - 2; k += 2){
				startSphere = CinputController.currentChain[k];
				middleSphere = CinputController.currentChain[k+1];
				endSphere = CinputController.currentChain[k+2];

				LineRenderer newLine = startSphere.GetComponent<LineRenderer>();
				if(newLine == null){
					newLine = startSphere.AddComponent<LineRenderer> ();
				}
				newLine.material = new Material(lineMaterial);
				newLine.material.SetColor("_Color",startSphere.renderer.material.color);
				newLine.SetWidth(width,width);

				if (numberOfPoints > 0){
					newLine.SetVertexCount(numberOfPoints);
				}

				// set points of quadratic Bezier curve
				p0 = startSphere.transform.position;
				p1 = middleSphere.transform.position;
				p2 = endSphere.transform.position;
				for(int i = 0; i < numberOfPoints; i++){
					t = i / (numberOfPoints - 1.0f);
					position = (1.0f - t) * (1.0f - t) * p0 + 2.0f * (1.0f - t) * t * p1 + t * t * p2;
					newLine.SetPosition(i, position);
				}
			}
		}

		if(CinputController.currentChain.Count > 3 && CinputController.currentChain.Count % 2 == 0){
			drawSingleSegment(CinputController.currentChain[CinputController.currentChain.Count - 2] , CinputController.currentChain[CinputController.currentChain.Count - 1]);
		}
	}

	void drawSingleSegment(GameObject start,GameObject end){		
		LineRenderer newLine = start.GetComponent<LineRenderer>();
		if(newLine == null){
			newLine = start.AddComponent<LineRenderer> ();
		}
		newLine.material = new Material(lineMaterial);
		newLine.material.SetColor("_Color",start.renderer.material.color);
		newLine.SetWidth(width,width);
		newLine.SetPosition (0,start.transform.position);
		newLine.SetPosition (1, end.transform.position);
	}
}
