﻿using UnityEngine;
using System.Collections;

public class GUIAlert {
	public string text;
	public Rect lastPos;
	public float ttl = 4.0f;
	public GUIStyle alertStyle;

	public GUIAlert(string newText){
		//this.alertStyle = GUIController.Instance.alertStyle;
		this.alertStyle = new GUIStyle ();
		this.alertStyle.fontSize = 80;
		this.alertStyle.fontStyle = FontStyle.Bold;
		this.text = newText;
		this.lastPos = new Rect (Screen.width / 2 - 20, Screen.height / 2 - 10, 40,20);
	}
}
