﻿using UnityEngine;
public class TimeChallenge : GameMode {
	public void onEndChain(){

	}

	public void onEndGame(){

	}

	public int generateScore(int score, int count){
		int increment = 0;
		if (count > 4) {
			increment += count * 5;
			TimeController.Instance.addToTime(Mathf.Round(count * .4f));
		}else if (count > 3) {
			increment += count * 3;
			TimeController.Instance.addToTime(1);
		}else if (count > 2) {
			increment += count * 2;
			//TimeController.Instance.addToTime(1);
		} else {
				increment = count;
		}
		return increment;
	}
}
