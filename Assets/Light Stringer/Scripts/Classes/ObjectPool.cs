﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPool {
	public ObjectPool(){}

	public int length = 0;

	private List<GameObject> pool = new List<GameObject>();

	public void add(GameObject obj){
		obj.SetActive (false);
		pool.Add(obj);
		length++;
	}

	public GameObject retrieve(){
		if(pool.Count > 0){
			GameObject obj = pool[0];
			pool.Remove (obj);
			length--;
			obj.SetActive(true);
			return obj;
		}
		else{
			return null;
		}
	}
}
