﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputController : Singleton<InputController> {
	public InputController(){}

	public List<GameObject> currentChain = new List<GameObject>();
	private Color listColor;
	private GameObject lastSphere;

	public Vector3 startDragPos;
	public bool creatingChain = false;

	public Shader lineMaterial;

	private LineDraw ClineDraw;

	void Start(){
		ClineDraw = GetComponent<LineDraw> ();
	}

	public void startChain(GameObject sphere){
		startDragPos = Input.mousePosition;
		creatingChain = true;
		this.addSphereToChain (sphere);
	}

	public void addSphereToChain(GameObject sphere){
		Color sphereColor = sphere.renderer.material.GetColor ("_Color");
		gamePiece currentGP = sphere.GetComponent<gamePiece>();
		// If first sphere in list, set list color
		if (currentChain.Count == 0) {
			listColor = sphereColor;
			// Add current sphere to chain
			currentChain.Add (sphere);
		} else if (sphereColor != listColor) {
				// Currently selected sphere does not match the chain color
				return;
		} else if (currentGP.selected) {
			// this sphere is already selected and in the chain
			return;
		}else {
			// Check if current sphere is next to last sphere
			gamePiece lastGP = lastSphere.GetComponent<gamePiece>();
			if((lastGP.X == currentGP.X && Mathf.Abs (lastGP.Y - currentGP.Y) == 1) || (lastGP.Y == currentGP.Y && Mathf.Abs (lastGP.X - currentGP.X) == 1)){
				// Is next to last piece
				// Add current sphere to chain
				currentChain.Add (sphere);
				// Draw line
				//drawLine (lastSphere, sphere);
				ClineDraw.draw();
			}
			else{
				// Is not next to last sphere
				return;
			}
		}

		lastSphere = sphere;
		currentGP.selected = true;

	}

	public void endChain(){
		creatingChain = false;

		// Check if there are multiple pieces in the current chain
		if (currentChain.Count > 1) {
			// Update score variables
			if(GameController.Instance.longestChain < currentChain.Count){
				GameController.Instance.longestChain = currentChain.Count;
			}
			GameController.Instance.totalPiecesRemoved += currentChain.Count;

			int increment = GameController.Instance.generateScore(GameController.Instance.score,currentChain.Count);
			GameController.Instance.score += increment;

			// Show alert
			GUIController.Instance.addAlertText (increment.ToString (), lastSphere);
			// Remove connected pieces
			this.removeChainPieces ();
			this.settleExistingPieces ();
		} else if(currentChain.Count == 1){
			// Only one piece in the current chain. Unset selected value for sphere and Clear it
			GameObject firstPiece = currentChain[0];
			gamePiece firstGP = firstPiece.GetComponent<gamePiece>();
			firstGP.selected = false;
			currentChain.Clear ();
		}
	}

	private void drawLine(GameObject startSphere, GameObject endSphere){
		LineRenderer newLine = startSphere.AddComponent<LineRenderer> ();
		newLine.material = new Material(lineMaterial);
		newLine.material.SetColor("_TintColor",new Color(1,0,1,0.3f));
		newLine.SetWidth(0.2f,0.2f);
		newLine.SetPosition (0,startSphere.transform.position);
		newLine.SetPosition (1, endSphere.transform.position);
	}

	private void removeChainPieces(){
		gamePiece pieceComponent;
		foreach(GameObject sphere in currentChain){
			// Add item to object pool and hide
			GameController.Instance.spherePool.add (sphere);
			// Remove it from grid
			pieceComponent = sphere.GetComponent<gamePiece>();
			pieceComponent.removeFromGrid();
			pieceComponent.selected = false;
			Destroy (sphere.GetComponent<LineRenderer>());
		}

		currentChain.Clear();
	}

	private void settleExistingPieces(){
		int xMax = GameController.Instance.gridWidth - 1;
		int yMax = GameController.Instance.gridHeight - 1;

		GameObject currentSphere;
		gamePiece currentPieceComponent;

		GameObject inspectingSphere;

		// Starting from second to last row of grid, move up row by row. 
		//If the current grid space has a sphere but the space below does not, move the sphere down until it's above a sphere or in the bottom row
		for (int row = yMax - 1; row >= 0; row--) {
			// Loop through columns
			for(int col = 0; col <= xMax; col++){
				// Check if this sphere is active and if it's already in the list of spheres to settle
				currentSphere = GameController.Instance.fetchSphere(col,row);
				// Check if sphere is in this location
				if(currentSphere != null){
					currentPieceComponent = currentSphere.GetComponent<gamePiece>();
					if(currentSphere.activeSelf && !GameController.Instance.isInSettlingList(currentPieceComponent)){
						inspectingSphere = GameController.Instance.fetchSphere(col,row + 1);
						if(inspectingSphere == null){
							// this sphere has an empty  space below, add it and all spheres above it
							GameController.Instance.addToSettlingList(currentPieceComponent);
							for(int offset = 1; row - offset >= 0; offset++){
								currentSphere = GameController.Instance.fetchSphere(col,row - offset);
								if(currentSphere != null && currentSphere.activeSelf){
									currentPieceComponent = currentSphere.GetComponent<gamePiece>();
									GameController.Instance.addToSettlingList(currentPieceComponent);
								}
							}
						}
					}
				}
			}
		}

		// If settling list is 0, no pieces need to fall. Immediately create new pieces
		if(GameController.Instance.settlingPiecesCount() == 0){
			GameController.Instance.showNewPieces();
		}
	}
}
