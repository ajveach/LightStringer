﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIController : Singleton<GUIController> {
	public GUIController() {}

	public bool isPhoneUI = true;
	public bool isTabletUI = false;
	public bool isDesktopUI = false;

	private PhoneUI phoneUI;

	public bool disabled = true;

	public bool showBoardResetDisplay = false;
	public float boardResetTimeRemaining = 3.0f;
	public float boardResetOpacity = 1.0f;

	public int lastScreenWidth;
	public int lastScreenHeight;

	public bool newHighScore = false;
	public bool newLongestChain = false;

	void Start(){
		phoneUI = GetComponent<PhoneUI> ();
	}

	public void addAlertText(string text, GameObject sphere){
		GUIAlert alert = new GUIAlert (text);
		Color alertColor = sphere.renderer.material.GetColor ("_Color");
		alertColor.a = 1;
		alert.alertStyle.normal.textColor = alertColor;
		if (isPhoneUI) {
			phoneUI.alerts.Add (alert);
		}
	}

	public void toggleBoardResetDisplay(){
		if (showBoardResetDisplay) {
			showBoardResetDisplay = false;
			GameController.Instance.endBoardReset();
		} else {
			showBoardResetDisplay = true;
			// Start coroutine to determine time and fade
			StartCoroutine("configureResetDisplay");
		}
	}

	IEnumerator configureResetDisplay () {
		boardResetTimeRemaining = 3.0f;
		boardResetOpacity = 1.0f;
		while (boardResetTimeRemaining > 0) {
			if(boardResetTimeRemaining % 2 == 0){
				boardResetOpacity = 1.0f;
			}
			boardResetTimeRemaining -= .1f;
			boardResetOpacity -= .1f;

			if(boardResetTimeRemaining <= 0){
				toggleBoardResetDisplay();
			}
			yield return new WaitForSeconds(.1f);
		}
	}

	void OnGUI(){
		if(!disabled){
			switch(Application.loadedLevelName){
				case "MainMenu":
					this.displayMainMenu();
					break;
				case "Play":
					this.displayPlayGUI();
					break;
			}
		}
	}

	void displayMainMenu(){
		if (isPhoneUI) {
			phoneUI.displayMainMenu();
		}
	}

	void displayPlayGUI(){
		if (isPhoneUI) {
			phoneUI.displayPlayGUI ();
		}
	}
	public bool screenIsSameSize(){
		if (lastScreenWidth == Screen.width && lastScreenHeight == Screen.height) {
			return true;
		}
		return false;
	}

}
