﻿using UnityEngine;
using System.Collections;

public class DemoSphereRotate : MonoBehaviour {
	private bool dragging = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(dragging){
			Vector3 pos = Input.mousePosition;
			pos.z = 10;
			transform.position = Camera.main.ScreenToWorldPoint(pos);
		}
		else{
			gameObject.transform.Rotate(Vector3.up);
		}
	}

	void OnMouseDown(){
		dragging = true;
	}

	void OnMouseUp(){
		dragging = false;
	}
}
