﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PhoneUI : MonoBehaviour {
	public int alertScrollSpeed = 40;
	public float alertFadeSpeed = 0.5f;

	public GUIStyle alertStyle;
	public GUIStyle gameOverStyle;
	public GUIStyle gameOverBackgroundStyle;
	public GUIStyle restartButtonStyle;
	public GUIStyle boardResetStyle;

	private GUIController guiController;

	public List<GUIAlert> alerts = new List<GUIAlert>();
	public List<GUIAlert> removeAlerts = new List<GUIAlert>();

	public GUIStyle MainMenuTitleStyle;
	public GUIStyle mainMenu_highScoreStyle;
	public GUIStyle MainMenuPlayButtonStyle;
	public GUIStyle MainMenuExitButtonStyle;
	public GUIStyle SEVENTY5TimesLogo;
	private int lastScreenWidth;
	private int lastScreenHeight;
	private bool showExitConfirmation = false;
	private Rect confirmationDialogRect;

	private Rect inGameScoreBoxPos;
	private Rect inGameScorePos;
	private Rect inGameTimerPos;
	public GUIStyle inGameScoreBoxStyle;
	public GUIStyle inGameScoreStyle;
	public GUIStyle inGameTimerStyle;

	public GUIStyle gameOverScoreStyle;
	public GUIStyle gameOverNewHighScoreStyle;
	public GUIStyle gameOverNewLongestChainStyle;

	void Awake(){
		MainMenuTitleStyle.fontSize = (int)(Screen.height * .1);
		MainMenuPlayButtonStyle.fontSize = (int)(Screen.height * .08);
		MainMenuExitButtonStyle.fontSize = (int)(Screen.height * .08);
		mainMenu_highScoreStyle.fontSize = (int)(Screen.height * 0.06f);
		SEVENTY5TimesLogo.fontSize = (int)(Screen.height * .04f);

		/* Calculate in game sizes based on resolution */
		inGameScoreBoxPos = new Rect (0, 0, Screen.width, Screen.height * 0.08f);
		float offset = Screen.width * 0.01f;
		inGameScorePos = new Rect (offset, offset, Screen.width - offset, Screen.height * 0.08f);
		inGameTimerPos = new Rect (0, offset, Screen.width - offset, Screen.height * 0.08f);
		inGameScoreStyle.fontSize = (int)(Screen.height * 0.05f);
		inGameTimerStyle.fontSize = (int)(Screen.height * 0.05f);

		/* Calcuate game over sizes based on resultion */
		gameOverStyle.fontSize = (int)(Screen.height * 0.1f);
		gameOverScoreStyle.fontSize = (int)(Screen.height * 0.05f);
		gameOverNewHighScoreStyle.fontSize = (int)(gameOverScoreStyle.fontSize * .5f);
		gameOverNewLongestChainStyle.fontSize = gameOverNewHighScoreStyle.fontSize;
	}

	void Start(){
		guiController = GetComponent<GUIController> ();
	}

	public void displayMainMenu(){
		// Check if sreen size has changed
		if (!guiController.screenIsSameSize ()) {
			this.mainMenu_recalcMenuStart();
		}

		// Print Title
		GUI.Label (new Rect(0,0,Screen.width,Screen.height * .14f),"Light Stringer",MainMenuTitleStyle);

		// Print current high score
		Rect pos = new Rect (0, Screen.height * 0.75f, Screen.width, Screen.height * 0.1f);
		GUI.Label (pos, "High Score: "+PlayerPrefs.GetInt ("highScore").ToString(),mainMenu_highScoreStyle);

		// Print main menu buttons
		// Start Game Button
		float spacing = Screen.width * 0.03f;
		if (GUI.Button (new Rect (spacing, Screen.height * .14f, Screen.width * .45f, Screen.height * .1f), "Play",MainMenuPlayButtonStyle)) {
			// Start Game
			Application.LoadLevel("Play");
		}
		// Options Button
		//GUI.Button (new Rect(menuStart.x,menuStart.y + 60, buttonWidth, 40),"Options");
		
		// Exit Button
		if (GUI.Button (new Rect (Screen.width * .55f - spacing, Screen.height * .14f, Screen.width * .45f, Screen.height * .1f), "Exit",MainMenuExitButtonStyle)) {
			showExitConfirmation = true;
		}

		// SEVENTY5Times logo
		GUI.Label (new Rect(Screen.width * .01f,Screen.height * .95f,Screen.width * .98f,Screen.height * .04f),"by SEVENTY5Times",SEVENTY5TimesLogo);

		if (showExitConfirmation) {
			displayExitConfirmation();
		}

		// In dev mode display button to clear high scores
		if (Debug.isDebugBuild) {
			if(GUI.Button (new Rect(0,Screen.height * .9f,Screen.width * .3f, Screen.height * .1f),"Clear Records")){
				PlayerPrefs.DeleteAll();
			}
		}
	}

	public void displayPlayGUI(){
		this.playGUI_printGUIAlerts ();
		this.printScoreDisplays ();
		this.printTimer ();
		this.printGameOverSplash ();
		this.printBoardResetDisplay();
	}

	private void mainMenu_recalcMenuStart(){
		guiController.lastScreenWidth = Screen.width;
		guiController.lastScreenHeight = Screen.height;
		
		// Calculate location and dimensions of confirmation dialog
		float dialogWidth = (float)(Screen.width * .6);
		float dialogHeight = 200.0f;
		confirmationDialogRect = new Rect (
			(float)((Screen.width - dialogWidth) / 2),
			(float)((Screen.height - dialogHeight) / 2),
			dialogWidth,
			dialogHeight
			);
	}

	private void displayExitConfirmation(){
		GUI.Box (confirmationDialogRect, "Are you sure you want to exit?");
		float buttonsTop = confirmationDialogRect.y + confirmationDialogRect.height - 60;
		if (GUI.Button (new Rect (confirmationDialogRect.x + 20, buttonsTop, 60, 40), "Yes")) {
			Application.Quit();
		}
		
		if (GUI.Button (new Rect (confirmationDialogRect.x + confirmationDialogRect.width - 80, buttonsTop, 60, 40), "No")) {
			showExitConfirmation = false;
		}
	}

	private void playGUI_printGUIAlerts(){
		foreach(GUIAlert alert in alerts){
			if(alert.ttl <= 0 || alert.alertStyle.normal.textColor.a <= 0){
				removeAlerts.Add(alert);
			}
			else{
				// Set new position for alert
				alert.lastPos = new Rect(alert.lastPos.x, alert.lastPos.y - alertScrollSpeed * Time.deltaTime, alert.lastPos.width, alert.lastPos.height);

				// Alter transparency for fadeout
				Color textColor = alert.alertStyle.normal.textColor;
				float newAlpha = textColor.a - alertFadeSpeed * Time.deltaTime;
				textColor.a = newAlpha;
				alert.alertStyle.normal.textColor = textColor;
				
				GUI.Label(alert.lastPos,alert.text,alert.alertStyle);
				alert.ttl -= Time.deltaTime;
			}
		}
		
		if (removeAlerts.Count > 0) {
			foreach(GUIAlert alert in removeAlerts){
				alerts.Remove (alert);
			}
			
			removeAlerts.Clear();
		}
	}
	
	private void printScoreDisplays(){
		GUI.Box (inGameScoreBoxPos, "",inGameScoreBoxStyle);
		GUI.Box (inGameScorePos, "Score: "+GameController.Instance.score.ToString (),inGameScoreStyle);
		//GUI.Label (new Rect (10, 30, 20, 20), GameController.Instance.score.ToString (), scoreStyle);
		
		//GUI.Box (new Rect (10, Screen.height - 50, 60, 50), "Longest Chain",scoreStyle);
		//GUI.Label (new Rect (10, Screen.height - 30, 60, 20), GameController.Instance.longestChain.ToString (), scoreStyle);
	}
	
	private void printTimer(){
		GUI.Box (inGameTimerPos, TimeController.Instance.current().ToString ()+"s to go!",inGameTimerStyle);
		//GUI.Label (new Rect (Screen.width - 30, 30, 20, 20), TimeController.Instance.current().ToString (), timerStyle);
	}
	
	private void printBoardResetDisplay(){
		if (guiController.showBoardResetDisplay) {
			GUI.Label(new Rect(Screen.width/2,Screen.height/2,100,30),Mathf.Ceil(guiController.boardResetTimeRemaining).ToString(),boardResetStyle);
		}
	}
	
	private void printGameOverSplash(){
		if (GameController.Instance.isOver ()) {
			GUI.Box (new Rect(0,0,Screen.width,Screen.height),"",gameOverBackgroundStyle);
			Rect restartRect = new Rect (Screen.width * 0.15f,Screen.height * 0.3f, Screen.width * 0.7f, Screen.height * 0.1f);
			Rect GORect = new Rect (0, Screen.height * 0.1f, Screen.width, Screen.height * 0.2f);
						
			if(GUI.Button (restartRect,"Touch here to try again",restartButtonStyle)){
				Application.LoadLevel("Play");
			}
			GUI.Label (GORect, "Game Over!",gameOverStyle);


			// Show score and longest chain
			GUI.Label(new Rect(Screen.width * 0.1f,Screen.height * 0.6f,Screen.width * 0.8f,Screen.height * 0.05f),"Score: "+GameController.Instance.score, gameOverScoreStyle);
			GUI.Label(new Rect(Screen.width * 0.1f,Screen.height * 0.7f,Screen.width * 0.8f,Screen.height * 0.05f),"Longest Chain: "+GameController.Instance.longestChain, gameOverScoreStyle);

			// Print new record alerts
			if(guiController.newHighScore){
				GUI.Label(new Rect(Screen.width * 0.6f, Screen.height * 0.55f, Screen.width * .3f, Screen.height * 0.05f),"New High Score!",gameOverNewHighScoreStyle);
			}

			if(guiController.newLongestChain){
				GUI.Label(new Rect(Screen.width * 0.6f, Screen.height * 0.65f, Screen.width * .3f, Screen.height * 0.05f),"New Longest Chain!",gameOverNewLongestChainStyle);
			}
		}
	}
}
