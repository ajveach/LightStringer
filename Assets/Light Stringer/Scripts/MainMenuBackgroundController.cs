﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainMenuBackgroundController : MonoBehaviour {
	public GameObject DemoSphere;
	public GameObject GC;
	private GameController GCComponent;

	public int sphereDistanceFromCenter = 3;

	public Vector3[] sphereRotationAxes;
	public float[] sphereRotationSpeeds;

	private List<GameObject> demoSpheres = new List<GameObject> ();
	private Color[] pieceColors;

	// Use this for initialization
	void Start () {
		GCComponent = GC.GetComponent<GameController>();

		// Instantiate 4 spheres
		GameObject newSphere;
		for(int i = 0; i < 4; i += 1){
			newSphere = Instantiate(DemoSphere, Random.onUnitSphere * sphereDistanceFromCenter, Quaternion.identity) as GameObject;
			newSphere.renderer.material.color = GCComponent.pieceColors[i];
			demoSpheres.Add(newSphere);

			sphereRotationSpeeds[i] = Random.Range (50,100);

			sphereRotationAxes[i] = new Vector3(Random.Range (0f,1f),Random.Range (0f,1f),Random.Range (0f,1f));
		}
	}
	
	// Update is called once per frame
	void Update () {
		int n = 0;
		foreach(GameObject sphere in demoSpheres){
			sphere.transform.RotateAround (Vector3.zero, sphereRotationAxes[n], sphereRotationSpeeds[n] * Time.deltaTime);
			n+=1;
		}
	}
}
